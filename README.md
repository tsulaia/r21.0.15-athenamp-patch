This project contains AthenaMP patches for running Event Service in rel 21.0.15

Instructions for building the patch (e.g. on lxplus):

```bash
setupATLAS
lsetup git
mkdir WorkDir
cd WorkDir
asetup 21.0.15,AtlasOffline,x86_64,slc6,gcc62,opt,here
git clone https://gitlab.cern.ch/tsulaia/r21.0.15-athenamp-patch source
mkdir build
cd build
cmake ../source
make
```

Once the build step finishes, you need to take two libraries from the `build` directory:
 * `x86_64-*/lib/libAthenaMP.so`
 * `x86_64-*/lib/libAthenaMPTools.so`

and use these libraries for patching 21.0.15 release
